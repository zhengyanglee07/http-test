<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        // Authentication
        $user = User::factory()->count(3)->create();
        $this->actingAs($user->first());

        // disabled laravel exception handling
        $this->withoutExceptionHandling();

        /***************************************
         *          Testing HTTP request       *
         ***************************************/
        // $response = $this->get('/test');
        // $this->assertTrue($response['value']);

        /* Customizing Request Headers */
        // $response = $this->withHeaders([
        //     'X-Header' => 'Value',
        // ])->get('/users');
        // $response->assertStatus(200);

        /* Cookies */
        // $response = $this->withCookie('color', 'blue')->get('/');

        /* Session */
        // $response = $this->withSession(['banned' => false])->get('/');


        /* Debugging Responses */
        // $response->dumpHeaders();
        // $response->dumpSession();
        // $response->dump();
        // $response->dd();

        // $response = $this->getJSON('/test', ['user' => 'abc']);

        /***************************************
         *           Testing JSON APIs         *
         ***************************************/
        // $response = $this->json('GET', '/test');
        // $this->assertTrue($response['created']);

        //* Asserting Exact JSON Matches 
        // $response
        //     ->assertStatus(200)
        //     ->assertExactJson([
        //         'property1' => 'value1',
        //         'property2' => 'value2',
        //         'property3' => 'value3',
        //         'property4' => 'value4',
        //         'property5' => 'value5'
        //     ]);

        //* JSON path
        // $response
        //     ->assertStatus(200)
        //     ->assertJsonPath('order.customer.firstname', 'abc');


        //* Fluent JSON
        // $response
        //     ->assertJson(
        //         fn (AssertableJson $json) =>
        //         $json
        //             // ->where('order.customer.firstname', 'abc')
        //             ->missing('user') 
        //             ->has('order') 
        //             ->hasAll('order', 'status')
        //             ->missingAll('a', 'b')
        //             ->hasAny('order', 'message', 'code')
        //             ->etc()
        //     );

        //* Asserting Against JSON Collections
        // $response = $this->get('/users/all');
        // $response
        //     ->assertJson(
        //         fn (AssertableJson $json) =>
        //         $json->has(3)
        //             // ->first(
        //             //     fn ($json) =>
        //             //     $json->where('id', 1)
        //             //         ->missing('password')
        //             //         ->etc()
        //             // )
        //             ->has(
        //                 2,
        //                 fn ($json) =>
        //                 $json->where('id', 3)
        //                     ->missing('password')
        //                     ->etc()
        //             )
        //     );

        //* JSON collections with named keys
        // $response
        //     ->assertJson(
        //         fn (AssertableJson $json) =>
        //         $json->has('meta')
        //             ->has(
        //                 'users',
        //                 3,
        //                 fn ($json) =>
        //                 $json->where('id', 1)
        //                     ->missing('password')
        //                     ->etc()
        //             )
        //     );

        //* Asserting JSON Types
        // $response->assertJson(
        //     fn (AssertableJson $json) =>
        //     $json
        //         ->whereType('status', ['string', 'integer'])->etc()
        //         ->whereAllType([
        //             'status' => ['string', 'integer'],
        //             'order' => 'array'
        //         ])
        // );

        /***************************************
         *          Testing File Uploads        *
         ***************************************/
        // Storage::fake('local');
        // $file = File::create('reports.pdf', 100);
        // $response = $this->post('/save/report', [
        //     'name' => $user->first()->name, 
        //     'file' => $file,
        // ]);

        // Storage::disk('local')->assertExists('reports/' .$file->hashName());


        /***************************************
         *             Testing Views            *
         ***************************************/
        // $view = $this->view('welcome', ['name' => 'ABC']);
        // $view->assertSee('ABC');
    }
}
