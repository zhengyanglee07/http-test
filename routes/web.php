<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['name' => 'ABC']);
});
Route::post('/view/error', 'App\Http\Controllers\TestController@viewsTesting');
Route::get('/test', 'App\Http\Controllers\TestController@index');
Route::post('/save/report', 'App\Http\Controllers\ReportsController@store');

Route::get('/users/all', function () {
    return User::all();
});

Route::get('/users', function () {
    return [
        'test' => 12345,
        'meta' => [],
        'users' => User::all(),
    ];
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
