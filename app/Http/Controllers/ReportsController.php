<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function store() {
        $report = Report::create([
            'name' => request('name'),
            'path' => request('file')->store('reports', 'local'),
        ]);

        return response($report);

    }
}
