<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $assertTrueData = ['created' => true];
        $assertExactJsonData = [
            'property1' => 'value1',
            'property2' => 'value2',
            'property3' => 'value3',
            'property4' => 'value4',
            'property5' => 'value5',
        ];
        $assertJsonPathData = [
            'order' => [
                'customer' => [
                    'firstname' => 'abc',
                    'lastname' => 'cba'
                ],
            ],
            'status' => 'pending',
        ];

        // return response('Hello World')->cookie(
        //     'property', 'value', 100
        // );

        return response($assertJsonPathData);
    }

    public function viewsTesting(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:reports|max:255',
        ]);

        return view('welcome');
    }
}
